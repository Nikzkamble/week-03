﻿using System;

namespace week_03
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //Start the program with Clear();
            Console.Clear();

            
           
             
             Console.WriteLine("Hello, please type in a number");

            var num1 = int.Parse(Console.ReadLine());

            Console.WriteLine($"The number you typed in multiplied by 2 is {num1 * 2}");
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
